CREATE SCHEMA tm;

SET
search_path TO tm;

CREATE TABLE IF NOT EXISTS tm_project
(
    id VARCHAR
(
    255
) PRIMARY KEY,
    user_id VARCHAR
(
    255
) NOT NULL,
    name VARCHAR
(
    255
) NOT NULL,
    description VARCHAR
(
    255
),
    status VARCHAR
(
    255
) NOT NULL,
    created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    start_date TIMESTAMPTZ
    );

CREATE TABLE IF NOT EXISTS tm_task
(
    id VARCHAR
(
    255
) PRIMARY KEY,
    user_id VARCHAR
(
    255
) NOT NULL,
    name VARCHAR
(
    255
) NOT NULL,
    description VARCHAR
(
    255
),
    status VARCHAR
(
    255
) NOT NULL,
    created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    start_date TIMESTAMPTZ,
    project_id VARCHAR
(
    255
)
    );

CREATE TABLE IF NOT EXISTS tm_user
(
    id VARCHAR
(
    255
) PRIMARY KEY,
    login VARCHAR
(
    255
) NOT NULL UNIQUE,
    password_hash VARCHAR
(
    255
) NOT NULL,
    role VARCHAR
(
    255
) NOT NULL,
    first_name VARCHAR
(
    255
),
    last_name VARCHAR
(
    255
),
    middle_name VARCHAR
(
    255
),
    email VARCHAR
(
    255
),
    lock boolean DEFAULT false NOT NULL
    );
