package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

@Component
public final class DisplayAboutServerListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display server developer info.";
    }

    @Override
    @EventListener(condition = "@displayAboutServerListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT SERVER]");
        System.out.println(systemEndpoint.getAbout(new ServerAboutRequest()).getName());
        System.out.println(systemEndpoint.getAbout(new ServerAboutRequest()).getEmail());
    }

}
