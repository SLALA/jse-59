package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class DisplayArgumentsListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Display arguments.";
    }

    @Override
    @EventListener(condition = "@displayArgumentsListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = commandService.getArguments();
        for (final AbstractListener command : commands) {
            @Nullable final String name = command.arg();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
