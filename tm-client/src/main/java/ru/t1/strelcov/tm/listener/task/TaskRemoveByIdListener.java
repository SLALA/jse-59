package ru.t1.strelcov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.removeByIdTask(new TaskRemoveByIdRequest(getToken(), id));
    }

}
