package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.configuration.ClientConfiguration;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.marker.IntegrationCategory;

public class SystemEndpointTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private static final ISystemEndpoint systemEndpoint = context.getBean(ISystemEndpoint.class);

    @Nullable
    private String token;

    @Before
    public void before() {
    }

    @After
    public void after() {
    }

    @Category(IntegrationCategory.class)
    @Test
    public void aboutTest() {
        Assert.assertNotNull(systemEndpoint.getAbout(new ServerAboutRequest()).getName());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void versionTest() {
        Assert.assertNotNull(systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}
