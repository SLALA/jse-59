package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint {

    @NotNull String SPACE = "http://endpoint.tm.strelcov.t1.ru/";
    @NotNull String PART = "AuthEndpointService";
    @NotNull String NAME = "AuthEndpoint";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IAuthEndpoint.class);
    }

    @WebMethod
    @NotNull
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request);

    @WebMethod
    @NotNull
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request);

    @WebMethod
    @NotNull
    UserProfileResponse getProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request);

}
