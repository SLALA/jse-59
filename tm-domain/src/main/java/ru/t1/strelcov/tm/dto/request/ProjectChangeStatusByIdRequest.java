package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String status;

    @Nullable
    private String id;

    public ProjectChangeStatusByIdRequest(@Nullable final String token, @Nullable final String id, @Nullable final String status) {
        super(token);
        this.status = status;
        this.id = id;
    }

}
