package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectRemoveByNameRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    public ProjectRemoveByNameRequest(@Nullable final String token, @Nullable final String name) {
        super(token);
        this.name = name;
    }

}
