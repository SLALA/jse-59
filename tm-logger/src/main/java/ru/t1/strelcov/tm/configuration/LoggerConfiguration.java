package ru.t1.strelcov.tm.configuration;

import com.mongodb.MongoClient;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.strelcov.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.strelcov.tm")
public class LoggerConfiguration {

    @Bean
    public ConnectionFactory connectionFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(propertyService.getMQConnectionFactory());
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    public MongoClient mongoClient(@NotNull final IPropertyService propertyService) {
        return new MongoClient(propertyService.getMongoHost(), propertyService.getMongoPort());
    }

}
