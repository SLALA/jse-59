package ru.t1.strelcov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    E add(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    E updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    E updateByName(@Nullable String userId, @Nullable String oldName, @Nullable String name, @Nullable String description);

    @NotNull
    E changeStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    E changeStatusByName(@Nullable String userId, @Nullable String oldName, @NotNull Status status);

    @NotNull
    List<E> findAll(@Nullable final String userId);

    @NotNull
    List<E> findAll(@Nullable final String userId, @Nullable final String sort);

    @NotNull
    E findByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    E removeByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    E findById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    E removeById(@Nullable final String userId, @Nullable final String id);

    void clear(@Nullable final String userId);

}
