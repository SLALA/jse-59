package ru.t1.strelcov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void add(@Nullable final E entity);

    void addAll(@Nullable final List<E> list);

    void clear();

    @NotNull
    E findById(@Nullable final String id);

    @NotNull
    E removeById(@Nullable final String id);

}
