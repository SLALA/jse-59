package ru.t1.strelcov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    List<Task> findAllTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

}
