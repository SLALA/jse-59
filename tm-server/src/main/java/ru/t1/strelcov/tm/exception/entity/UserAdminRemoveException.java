package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UserAdminRemoveException extends AbstractException {

    public UserAdminRemoveException() {
        super("Error: You cannot remove main administrator \"admin\"");
    }

}
