package ru.t1.strelcov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

@Repository
public final class ProjectDTORepository extends AbstractBusinessDTORepository<ProjectDTO> implements IProjectDTORepository {

    @Override
    @NotNull
    public Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
