package ru.t1.strelcov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.model.IRepository;
import ru.t1.strelcov.tm.model.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @PersistenceContext
    @NotNull
    protected EntityManager entityManager;

    @NotNull
    public abstract Class<E> getClazz();

    @NotNull
    public String getEntityName() {
        return getClazz().getSimpleName();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final String jpql = "FROM " + getEntityName();
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public void addAll(@NotNull final List<E> entities) {
        entities.forEach((e) -> {
            add(e);
            entityManager.flush();
        });
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getEntityName();
        entityManager.createQuery(jpql).executeUpdate();
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
