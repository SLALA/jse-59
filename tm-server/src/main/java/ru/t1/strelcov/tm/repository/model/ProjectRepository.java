package ru.t1.strelcov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.model.Project;

@Repository
public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    public Class<Project> getClazz() {
        return Project.class;
    }

}
