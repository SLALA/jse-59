package ru.t1.strelcov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.model.User;

import java.util.Optional;

@Repository
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public Class<User> getClazz() {
        return User.class;
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.login = :login";
        return entityManager.createQuery(jpql, getClazz()).setParameter("login", login).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

    @SneakyThrows
    @Override
    public boolean loginExists(@NotNull final String login) {
        @NotNull final String jpql = "SELECT COUNT(e) > 0 FROM " + getEntityName() + " e WHERE e.login = :login";
        return entityManager.createQuery(jpql, Boolean.class).setParameter("login", login).getSingleResult();
    }

}
