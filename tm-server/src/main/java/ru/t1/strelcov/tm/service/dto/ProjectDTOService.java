package ru.t1.strelcov.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.strelcov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.repository.dto.ProjectDTORepository;
import ru.t1.strelcov.tm.repository.dto.TaskDTORepository;

import java.util.Optional;

@Getter
@Service
public class ProjectDTOService extends AbstractBusinessDTOService<ProjectDTO> implements IProjectDTOService {

    @Autowired
    private ProjectDTORepository repository;

    @Autowired
    private TaskDTORepository taskRepository;

    @Transactional
    @NotNull
    @Override
    public ProjectDTO add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final ProjectDTO project;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            project = new ProjectDTO(userId, name, description);
        else
            project = new ProjectDTO(userId, name);
        add(project);
        return project;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public ProjectDTO removeProjectWithTasksById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final IProjectDTORepository projectRepository = getRepository();
        taskRepository.removeAllByProjectId(userId, projectId);
        @NotNull final ProjectDTO project = Optional.ofNullable(projectRepository.removeById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
        return project;
    }

}
