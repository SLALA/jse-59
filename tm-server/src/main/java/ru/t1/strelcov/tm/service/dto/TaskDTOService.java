package ru.t1.strelcov.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.repository.dto.TaskDTORepository;

import java.util.List;
import java.util.Optional;

@Getter
@Service
public class TaskDTOService extends AbstractBusinessDTOService<TaskDTO> implements ITaskDTOService {

    @Autowired
    private TaskDTORepository repository;

    @Transactional
    @NotNull
    @Override
    public TaskDTO add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        final TaskDTO task;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            task = new TaskDTO(userId, name, description);
        else
            task = new TaskDTO(userId, name);
        add(task);
        return task;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<TaskDTO> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final ITaskDTORepository taskRepository = getRepository();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public TaskDTO bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final ITaskDTORepository taskRepository = getRepository();
        @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(projectId);
        taskRepository.update(task);
        return task;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final ITaskDTORepository taskRepository = getRepository();
        @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(null);
        taskRepository.update(task);
        return task;
    }

}
