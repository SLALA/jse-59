package ru.t1.strelcov.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.model.IProjectService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.ProjectRepository;
import ru.t1.strelcov.tm.repository.model.UserRepository;

import java.util.Optional;

@Getter
@Service
public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @Autowired
    private ProjectRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @NotNull
    @Override
    public Project add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IUserRepository userRepository = getUserRepository();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(userId)).orElseThrow(EntityNotFoundException::new);
        final Project project;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            project = new Project(user, name, description);
        else
            project = new Project(user, name);
        projectRepository.add(project);
        return project;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public Project removeProjectWithTasksById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final Project project = Optional.ofNullable(projectRepository.removeById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
        return project;
    }

}
