package ru.t1.strelcov.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.model.ITaskRepository;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.model.ITaskService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.ProjectRepository;
import ru.t1.strelcov.tm.repository.model.TaskRepository;
import ru.t1.strelcov.tm.repository.model.UserRepository;

import java.util.List;
import java.util.Optional;

@Getter
@Service
public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @NotNull
    @Override
    public Task add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final IUserRepository userRepository = getUserRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(userId)).orElseThrow(EntityNotFoundException::new);
        @NotNull final Task task;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            task = new Task(user, name, description);
        else
            task = new Task(user, name);
        taskRepository.add(task);
        return task;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
        task.setProject(project);
        taskRepository.update(task);
        return task;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProject(null);
        taskRepository.update(task);
        return task;
    }

}
