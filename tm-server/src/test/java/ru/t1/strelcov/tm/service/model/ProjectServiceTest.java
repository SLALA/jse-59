package ru.t1.strelcov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.service.model.IProjectService;
import ru.t1.strelcov.tm.api.service.model.ITaskService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.configuration.ServerTestConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerTestConfiguration.class);

    @NotNull
    private static final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private static final IProjectService service = context.getBean(IProjectService.class);

    @NotNull
    private static final ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private static final User testUser = new User("test2", "test", Role.USER);

    @NotNull
    private static final User testUserUnique = new User("testUnique", "test", Role.USER);

    @NotNull
    private static final String testUserId = testUser.getId();

    @NotNull
    private static final String testUserUniqueId = testUserUnique.getId();

    @NotNull
    private static final String NAME_UNIQUE = "NAME_UNIQUE";

    @NotNull
    private static final List<Project> projects = Arrays.asList(
            new Project(testUser, "pr1"),
            new Project(testUser, "pr4"),
            new Project(testUser, "pr3"));

    @Before
    public void before() {
        try {
            @NotNull User existedUser = userService.findByLogin(testUser.getLogin());
            taskService.clear(existedUser.getId());
            service.clear(existedUser.getId());
            userService.removeByLogin(existedUser.getLogin());
        } catch (@NotNull Exception ignored) {
        }
        try {
            @NotNull User existedUser = userService.findByLogin(testUserUnique.getLogin());
            taskService.clear(existedUser.getId());
            service.clear(existedUser.getId());
            userService.removeByLogin(existedUser.getLogin());
        } catch (@NotNull Exception ignored) {
        }
        userService.add(testUser);
        userService.add(testUserUnique);
        service.addAll(projects);
    }

    @After
    public void after() {
        taskService.clear(testUserId);
        taskService.clear(testUserUniqueId);
        service.clear(testUserId);
        service.clear(testUserUniqueId);
        userService.removeById(testUserId);
        userService.removeById(testUserUniqueId);
    }

    @AfterClass
    public static void afterClass() {
        context.close();
    }

    @Test
    public void addTest() {
        @NotNull final Project project = new Project(testUser, "pr1");
        int size = service.findAll(testUserId).size();
        service.add(null);
        Assert.assertEquals(size, service.findAll(testUserId).size());
        service.add(project);
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(project));
    }

    @Test
    public void addAllTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(testUser, "pr11"), new Project(testUser, "pr22")};
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        int size = service.findAll(testUserId).size();
        int listSize = list.size();
        @NotNull final ArrayList<Project> listWithNulls = new ArrayList<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).containsAll(list));
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(project.getId())));
            Assert.assertEquals(project, service.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(project.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void updateByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            @NotNull final String id = project.getId();
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(null, id, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, id, "", "newDesc"));
            service.updateById(userId, id, "newName", "newDesc");
            Assert.assertEquals("newName", service.findById(userId, id).getName());
            Assert.assertEquals("newDesc", service.findById(userId, id).getDescription());
            Assert.assertNotNull(service.updateById(userId, id, "newName", null));
            Assert.assertNull(service.findById(userId, id).getDescription());
        }
    }

    @Test
    public void updateByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            @NotNull final String name = project.getName();
            @Nullable final String newName = "newName";
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, null, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(null, name, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, name, null, "newDesc"));
            service.updateByName(userId, name, newName, "newDesc");
            Assert.assertEquals("newDesc", service.findByName(userId, newName).getDescription());
            Assert.assertEquals(newName, service.findById(project.getId()).getName());
        }
    }

    @Test
    public void changeStatusByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            @NotNull final String id = project.getId();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(null, id, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(userId, null, status));
                service.changeStatusById(userId, id, status);
                Assert.assertEquals(status, service.findById(userId, id).getStatus());
            }
        }
    }

    @Test
    public void changeStatusByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            @NotNull final String name = project.getName();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(null, name, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, status));
                service.changeStatusByName(userId, name, status);
                Assert.assertEquals(status, service.findByName(userId, name).getStatus());
            }
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(testUserUnique, "pr2"), new Project(testUserUnique, "pr1")};
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null));
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        @NotNull final String userId = list.get(0).getUser().getId();
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        Assert.assertEquals(list.size(), service.findAll(userId).size());
        Assert.assertTrue(list.containsAll(service.findAll(userId)));
    }

    @Test
    public void findAllSortedByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(testUserUnique, "pr2"), new Project(testUserUnique, "pr1")};
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        @NotNull Comparator<Project> comparator = SortType.NAME.getComparator();
        @NotNull String sort = SortType.NAME.name();
        @Nullable String userId = list.get(0).getUser().getId();
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null, sort));
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        @NotNull final List<Project> sortedList = service.findAll(userId, sort);
        Assert.assertEquals(list.size(), sortedList.size());
        Assert.assertTrue(list.containsAll(sortedList));
        Assert.assertEquals(sortedList.stream().sorted(comparator)
                .collect(Collectors.toList()), sortedList);
    }

    @Test
    public void findByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            @NotNull final String name = project.getName();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(userId, name)));
            Assert.assertEquals(project.getName(), service.findByName(userId, name).getName());
            Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", name));
            Assert.assertThrows(AbstractException.class, () -> service.findByName(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByName("notExistedId", null));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeByName(project.getUser().getId(), project.getName()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void findByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUser().getId();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(userId, project.getId())));
            Assert.assertEquals(project, service.findById(userId, project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(project.getUser().getId(), project.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void removeByIdByUserIdWithTasksTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        @NotNull final Task task = taskService.add(project.getUser().getId(), "task1", "descr1");
        taskService.bindTaskToProject(task.getUser().getId(), task.getId(), project.getId());
        Assert.assertEquals(1, taskService.findAllTasksByProjectId(project.getUser().getId(), project.getId()).size());
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeProjectWithTasksById(project.getUser().getId(), project.getId()));
        Assert.assertFalse(taskService.findAll(task.getUser().getId()).contains(task));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void clearByUserIdTest() {
        final String userId = testUserId;
        Assert.assertThrows(AbstractException.class, () -> service.clear(null));
        @NotNull final List<Project> list = service.findAll(userId);
        int size = list.size();
        int fullSize = service.findAll().size();
        service.clear(userId);
        service.clear("notExistedId");
        Assert.assertEquals(0, service.findAll(userId).size());
        Assert.assertEquals(fullSize - size, service.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", "descr_new"));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, "descr_new"));
        int size = service.findAll(testUserId).size();
        Assert.assertNotNull(service.add(testUserId, NAME_UNIQUE, "descr_new"));
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(testUserId, NAME_UNIQUE)));
    }

}
