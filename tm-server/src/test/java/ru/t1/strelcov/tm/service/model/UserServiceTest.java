package ru.t1.strelcov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.configuration.ServerTestConfiguration;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerTestConfiguration.class);

    @NotNull
    private static final IUserService service = context.getBean(IUserService.class);

    @NotNull
    private static final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @NotNull
    private static final String LOGIN_UNIQUE = "Login_Unique";

    @NotNull
    private static final String PASS = "PASS";

    @NotNull
    private static final String PASS_HASH = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);

    @NotNull
    private static final List<User> users = Arrays.asList(
            new User("test1", PASS_HASH, Role.USER),
            new User("test2", PASS_HASH, Role.ADMIN));

    @Before
    public void before() {
        for (@NotNull final User user : users) {
            try {
                @NotNull User existedUser = service.findByLogin(user.getLogin());
                service.removeByLogin(existedUser.getLogin());
                service.removeByLogin(LOGIN_UNIQUE);
            } catch (@NotNull Exception ignored) {
            }
        }
        try {
            service.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
        service.addAll(users);
    }

    @After
    public void after() {
        try {
            for (@NotNull final User user : users)
                service.removeById(user.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            service.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
    }

    @AfterClass
    public static void afterClass() {
        context.close();
    }

    @Test
    public void addTest() {
        @NotNull final User user = new User(LOGIN_UNIQUE, "pr1");
        int size = service.findAll().size();
        service.add(null);
        Assert.assertEquals(size, service.findAll().size());
        service.add(user);
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(user));
    }

    @Test
    public void addAllTest() {
        for (@NotNull final User user : users)
            service.removeById(user.getId());
        @NotNull final List<User> list = users;
        int size = service.findAll().size();
        int listSize = list.size();
        @NotNull final List<User> listWithNulls = new ArrayList<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll().size());
        Assert.assertTrue(service.findAll().containsAll(list));
    }

    @Test
    public void findAllTest() {
        Assert.assertTrue(service.findAll().containsAll(users));
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final User user : service.findAll()) {
            Assert.assertTrue(service.findAll().contains(service.findById(user.getId())));
            Assert.assertEquals(user, service.findById(user.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final User user = users.get(0);
        int fullSize = service.findAll().size();
        Assert.assertNotNull(service.removeById(user.getId()));
        Assert.assertFalse(service.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void addWithRole() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", Role.ADMIN));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, Role.ADMIN));
        int size = service.findAll().size();
        @Nullable User user = service.add(LOGIN_UNIQUE, PASS, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(user));
        Assert.assertEquals(LOGIN_UNIQUE, user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        @NotNull String expectedPassHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(""));
        service.add(LOGIN_UNIQUE, PASS, Role.ADMIN);
        @Nullable User user = service.findByLogin(LOGIN_UNIQUE);
        Assert.assertNotNull(user);
        Assert.assertEquals(LOGIN_UNIQUE, user.getLogin());
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin(""));
        int fullSize = service.findAll().size();
        @Nullable User user = service.removeByLogin(users.get(0).getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(service.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void updateByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin("notExistedId", "name", "fam", "", "email"));
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin(null, "name", "fam", "", "email"));
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin("", "name", "fam", "", "email"));
        service.updateByLogin(users.get(0).getLogin(), "name", "fam", "otch", "email");
        @Nullable User user = service.findByLogin(users.get(0).getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals("name", user.getFirstName());
        Assert.assertEquals("fam", user.getLastName());
        Assert.assertEquals("email", user.getEmail());
        Assert.assertEquals("otch", user.getMiddleName());
    }

    @Test
    public void changePasswordById() {
        Assert.assertThrows(AbstractException.class, () -> service.changePasswordById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.changePasswordById(null, "name"));
        @Nullable User user = users.get(0);
        Assert.assertNotNull(user);
        @NotNull final String id = user.getId();
        @NotNull final String newPass = PASS + "new";
        @NotNull String expectedPassHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), newPass);
        service.changePasswordById(id, newPass);
        user = service.findById(user.getId());
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
    }

    @Test
    public void lockUnlockByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin("notExistedId"));
        for (@NotNull User user : users) {
            @NotNull final String login = user.getLogin();
            @NotNull final Role role = user.getRole();
            if (role == Role.ADMIN)
                Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(login));
            else {
                service.lockUserByLogin(login);
                user = service.findById(user.getId());
                Assert.assertEquals(true, user.getLock());
                service.unlockUserByLogin(login);
                user = service.findById(user.getId());
                Assert.assertEquals(false, user.getLock());
            }
        }
    }

}
